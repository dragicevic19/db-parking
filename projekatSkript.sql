CREATE TABLE ULICA(
	 sifraUlice integer NOT NULL,
	 nazivUlice varchar2(50) NOT NULL,
	 CONSTRAINT ulica_PK PRIMARY KEY (sifraUlice)
);

CREATE TABLE PARKIRALISTE(
	 sifraPar integer NOT NULL,
	 brParkMesta integer NOT NULL,
     sifraUlice integer NOT NULL,
	 CONSTRAINT parkiraliste_PK PRIMARY KEY (sifraPar),
     CONSTRAINT parkiraliste_ulica_FK FOREIGN KEY (sifraUlice) REFERENCES ULICA (sifraUlice)
);

CREATE TABLE VOZILO(
    regOzn varchar2(10) NOT NULL,
    marka varchar2(20),
    model varchar2(20),
    CONSTRAINT vozilo_PK PRIMARY KEY (regOzn)
);

CREATE TABLE PARKIRANO_NA(
    sifraPar integer NOT NULL,
    regOzn varchar2(10) NOT NULL,
    datumPark DATE DEFAULT SYSDATE,
    CONSTRAINT parkirano_na_PK PRIMARY KEY (sifraPar, regOzn),
    CONSTRAINT parkirano_na_park_FK FOREIGN KEY (sifraPar) REFERENCES PARKIRALISTE(sifraPar),
    CONSTRAINT parkirano_na_vozilo_FK FOREIGN KEY (regOzn) REFERENCES VOZILO(regOzn)
);
COMMIT;


INSERT INTO ULICA values (1, 'Bulevar Evrope');
INSERT INTO ULICA values (2, 'Bulevar kralja Petra I');
INSERT INTO ULICA values (3, 'Gunduliceva');
INSERT INTO ULICA values (4, 'Temerinska');
INSERT INTO ULICA values (5, 'Kosovska');
INSERT INTO ULICA values (6, 'Bulevar oslobodjenja');
INSERT INTO ULICA values (7, 'Pavla papa');
INSERT INTO ULICA values (8, 'Novosadskog sajma');
INSERT INTO ULICA values (9, 'Dunavska');
INSERT INTO ULICA values (10, 'Vojvode Misica');
COMMIT;

INSERT INTO PARKIRALISTE values ( 1, 700,  5);
INSERT INTO PARKIRALISTE values ( 2,  25, 10);
INSERT INTO PARKIRALISTE values ( 3, 500,  2);
INSERT INTO PARKIRALISTE values ( 4, 100,  4);
INSERT INTO PARKIRALISTE values ( 5,  60,  9);
INSERT INTO PARKIRALISTE values ( 6, 135,  8);
INSERT INTO PARKIRALISTE values ( 7,  35,  7);
INSERT INTO PARKIRALISTE values ( 8, 800,  6);
INSERT INTO PARKIRALISTE values ( 9, 200,  1);
INSERT INTO PARKIRALISTE values (10,  60,  3);
COMMIT;

INSERT INTO VOZILO values ('NS100LO', 'SKODA', 'Octavia');
INSERT INTO VOZILO values ('NS001AA', 'BMW', '3');
INSERT INTO VOZILO values ('NS002BB', 'FIAT', 'Bravo');
INSERT INTO VOZILO values ('NS003CC', 'AUDI', 'A5');
INSERT INTO VOZILO values ('NS004DD', 'AUDI', 'A7');
INSERT INTO VOZILO values ('NS005EE', 'BMW', '5');
INSERT INTO VOZILO values ('NS006FF', 'SEAT', 'Leon');
INSERT INTO VOZILO values ('NS007GG', 'RENAULT', 'Megane');
INSERT INTO VOZILO values ('NS008HH', 'RENAULT', 'Clio');
INSERT INTO VOZILO values ('NS009II', 'AUDI', 'A6');
COMMIT;

INSERT INTO PARKIRANO_NA (sifraPar, regOzn) values (3, 'NS100LO');
INSERT INTO PARKIRANO_NA (sifraPar, regOzn) values (3, 'NS005EE');
INSERT INTO PARKIRANO_NA values (3, 'NS003CC', to_date('01-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (5, 'NS007GG', to_date('02-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (1, 'NS004DD', to_date('02-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (9, 'NS009II', to_date('02-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (8, 'NS001AA', to_date('03-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (8, 'NS002BB', to_date('04-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (8, 'NS006FF', to_date('06-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (8, 'NS008HH', to_date('05-06-2021', 'DD-MM-YYYY'));
INSERT INTO PARKIRANO_NA values (11,'NS009II', to_date('06-06-2021', 'DD-MM-YYYY'));
COMMIT;


--izmene definicije 
ALTER TABLE PARKIRALISTE
ADD (
    dnevnaKarta NUMBER(1) DEFAULT 0
);
COMMIT;
INSERT INTO PARKIRALISTE (sifraPar,brParkMesta, sifraUlice) values (11, 300,  6);


ALTER TABLE PARKIRALISTE
ADD CONSTRAINT parkiraliste_ch CHECK (brParkMesta > 5); 
COMMIT;

--update
UPDATE PARKIRALISTE
SET dnevnaKarta = 1
WHERE sifraPar in (1,2,3,4,5);
COMMIT;

UPDATE PARKIRALISTE
SET dnevnaKarta = 1
where sifraPar = 8;
ROLLBACK; -- vise parkiraliste 8 nema dnevnu kartu

--upiti
SELECT sifraPar "Sifra parkiralista" , regOzn "Registarska oznaka", datvremepark "Datum i vreme parkiranja"
FROM PARKIRANO_NA
WHERE regOzn like 'NS%';

-- izlistati sifru parkiralista koje se nalazi u ulici sa sifrom 9
-- ili ima vise od prosecnog broja parking mesta    (D1)
SELECT p.sifraPar "Sifra parkiralista"
FROM PARKIRALISTE p
WHERE p.sifraulice = 9
union
SELECT p.sifraPar "Sifra parkiralista"
FROM PARKIRALISTE p
where p.brparkmesta > (select avg(brParkMesta) from PARKIRALISTE);

--izlistati registarske oznake, marke i modele vozila i ulicu u kojoj je parkirano to vozilo
SELECT v.regOzn "Registarska oznaka", v.marka "Marka", v.model "Model", u.nazivUlice "Parkirano u ulici" 
FROM VOZILO v 
INNER JOIN PARKIRANO_NA pn 
    ON pn.regOzn = v.regOzn
INNER JOIN PARKIRALISTE p
    ON pn.sifraPar = p.sifraPar
INNER JOIN ULICA u
    ON p.sifraUlice = u.sifraUlice;
    
--izlistati sifre parkiralista sa sifrom > 5 na kojima su bar 3 vozila parkirana
SELECT pn.sifraPar "Sifra parkiralista", count(pn.regOzn) "Broj parkiranih vozila"
FROM PARKIRANO_NA pn
WHERE pn.sifraPar > 5
GROUP BY pn.sifraPar
HAVING COUNT(pn.regOzn) > 2;

--izlistati sve reg oznake vozila i sifru parkiralista na kojima su parkirana kao i preostali broj parkiranih automobila(pored njih)
WITH parkinfo AS(
    SELECT sifraPar, COUNT(regOzn) br_parkiranih
    FROM PARKIRANO_NA
    GROUP BY sifraPar
)
SELECT  v.regOzn "Registarska oznaka", pn.sifraPar "Sifra parkiralista", pi.br_parkiranih-1 "Preostalo automobila"
FROM VOZILO v, PARKIRANO_NA pn, PARKINFO pi
WHERE v.regOzn = pn.regOzn AND pn.sifraPar = pi.sifraPar;

--prikazuje koliko na svakom parkiralistu ima vozila
CREATE OR REPLACE VIEW
zauzece_parkinga (sifraPar, brVozila) AS
SELECT p.sifraPar, COUNT(pn.regOzn)
FROM PARKIRALISTE p, PARKIRANO_NA pn
WHERE p.sifraPar=pn.sifraPar(+)
GROUP BY p.sifraPar;

set serveroutput on;

CREATE OR REPLACE TRIGGER Trg_Vozilo_RegOzn_UPD
BEFORE UPDATE OF regOzn ON VOZILO
FOR EACH ROW
    WHEN (NEW.regOzn != OLD.regOzn)
    BEGIN
        raise_application_error(-20000, 'Registarski broj vozila se ne sme menjati');
END Trg_Vozilo_RegOzn_UPD;
/

UPDATE VOZILO
SET regOzn = 'LO001NS'
WHERE regOzn = 'NS100LO';
/

UNDEFINE naziv_ulice;
-- ispisuje vozila i datume parkiranja na svim parkiralistima u unetoj ulici
ACCEPT naziv_ulice PROMPT 'Unesite naziv ulice: ';
DECLARE
    TYPE T_parkiraliste_slog IS RECORD(
        sifraPar PARKIRALISTE.sifraPar%TYPE,
        regOzn   VOZILO.regOzn%TYPE,
        datumPark PARKIRANO_NA.datumPark%TYPE
    );
    
    TYPE T_parkiralista IS TABLE OF T_parkiraliste_slog INDEX BY BINARY_INTEGER;
    
    TYPE T_rec IS RECORD(
        nazivUlice ULICA.nazivUlice%TYPE,
        parkiralista T_parkiralista
    );
    
    TYPE T_ulice IS TABLE OF T_rec INDEX BY BINARY_INTEGER;
    ulice T_ulice;
    
    CURSOR c (idUlice ULICA.sifraUlice%TYPE) IS 
    SELECT p.sifraPar sifraPar, pn.regOzn regOzn, pn.datumPark datumPark
    FROM PARKIRALISTE p, PARKIRANO_NA pn
    WHERE p.sifraUlice = idUlice and p.sifraPar = pn.sifraPar;
    
    pronadjeno NUMBER;
    i NUMBER;
    j NUMBER;

BEGIN
    i := 0;
    FOR ul IN (SELECT * FROM ULICA) LOOP
        ulice(i).nazivUlice := ul.nazivUlice;
        j := 0;
        FOR parNa IN c(ul.sifraUlice) LOOP
            ulice(i).parkiralista(j).sifraPar := parNa.sifraPar;
            ulice(i).parkiralista(j).regOzn := parNa.regOzn;
            ulice(i).parkiralista(j).datumPark := parNa.datumPark;
            j := j + 1;
        END LOOP;
        i := i + 1;
    END LOOP;
    
    SELECT COUNT(1) INTO pronadjeno
    FROM ULICA
    WHERE nazivUlice = '&&naziv_ulice';
    
    IF pronadjeno > 0 THEN
        FOR i IN ulice.FIRST .. ulice.LAST LOOP
            IF ulice(i).nazivUlice = '&&naziv_ulice' THEN
                DBMS_OUTPUT.PUT_LINE('U ulici ' || ulice(i).nazivUlice || ' su bila parkirana sledeca vozila:');
                
                FOR j IN ulice(i).parkiralista.FIRST .. ulice(i).parkiralista.LAST LOOP
                    DBMS_OUTPUT.PUT_LINE('    Parkiraliste: ' || ulice(i).parkiralista(j).sifraPar);
                    DBMS_OUTPUT.PUT_LINE('    Parkirano vozilo: ' || ulice(i).parkiralista(j).regOzn);
                    DBMS_OUTPUT.PUT_LINE('    Datum: ' || ulice(i).parkiralista(j).datumPark);
                    DBMS_OUTPUT.PUT_LINE('');

                END LOOP;
            END IF;    
        END LOOP;
    
    ELSE
        DBMS_OUTPUT.PUT_LINE('O unetoj ulici nemamo podatke');
    END IF;
END;
/

